package PP2;
import java.util.InputMismatchException;
import java.lang.NumberFormatException;
import java.util.Scanner;
public class BangunRuang {
	private static double Volume;
	public static void main(String[] args){
		Menu CoreMenu = new Menu();
		int menu1=1;
		do{
			System.out.println("");
			System.out.println("menu");
			System.out.println("1. Bangun datar");
			System.out.println("2. Bangun ruang");
			System.out.println("0. Keluar.");
			int selection = Input.input();
			switch(selection){
				case 1:
					CoreMenu.MenuBangunDatar();
					break;
				case 2:
					CoreMenu.MenuBangunRuang();
					break;
				default:
					System.out.println("Inputan salah!");
					menu1 = 1;
			}
		}
		while(menu1 >= 1);
			System.out.println("Terima kasih atas kerja samanya :)");
	}
	public void setCube(double Volume){
		this.Volume=Volume;
		}
	public static double getCube(double side){
		System.out.print("Side = ");
		side = Input.input();
		Volume = side * side * side;
		System.out.print("Result = ");
		return Volume;
		}
	
	public void setBeam(double Volume){
		this.Volume=Volume;
		}
	public static double getBeam(double length, double width,double height){
		System.out.print("Length = ");
		length = Input.input();
		System.out.print("Width = ");
		width = Input.input();
		System.out.print("Height = ");
		height = Input.input();
		Volume = length * width * height;
		System.out.print("Result = ");
		return Volume;
		}
	
	public void setTube(double Volume){
		this.Volume=Volume;
		}
	public static double getTube(double PHI, double r, double height){
		Scanner sc = new Scanner(System.in);
		PHI = 3.14;
		System.out.print("Fingers = ");
		r = Input.input();
		System.out.print("Height = ");
		height = Input.input();
		Volume= PHI * r * r * height;
		System.out.print("Result = ");
		return Volume;
		}
	
	public void setBall(double Volume){
		this.Volume=Volume;
		}
	public static double getBall(double PHI, double r){
		PHI = 3.14;
		System.out.print("Fingers = ");
		r = Input.input();
		Volume= (4 * PHI * r * r)/3;
		System.out.print("Result = ");
		return Volume;
		}
	
	public void setCone(double Volume){
		this.Volume=Volume;
		}
	public static double getCone(double PHI, double r, double height){
		PHI = (float) 3.14;
		System.out.print("Fingers = ");
		r = Input.input();
		System.out.print("Height = ");
		height = Input.input();
		Volume= (PHI * r * r * height)/3;
		System.out.print("Result = ");
		return Volume;
		}
	
	public void setPyramid(double Volume){
		this.Volume=Volume;
		}
	public static double getPyramid(double side, double height){
		System.out.print("Side = ");
		side = Input.input();
		System.out.print("Height = ");
		height = Input.input();
		Volume= (Math.pow(side,2)*height)/3;
		System.out.println("Result = ");
		return Volume;
		}
	}

class BangunDatar{
	private static double area;
	private static double around;
	public static void circle(){
		double r, phi = 3.14;
		System.out.println("\n–> lingkaran <–");
		System.out.print("Masukkan Panjang Jari-jari : ");
		r = Input.input();
		System.out.println("=>");
		area= phi * r * r;
		around= 2 * phi * r;
		System.out.print("Luas  = "+area);
		System.out.println("");
		System.out.print("Keliling = "+around);
}
	
	public static void square(){
		double side;
		System.out.println("\n–>Persegi<–");
		System.out.print("Masukkan Panjang sisi : ");
		side = Input.input();
		System.out.println("=>");
		area = side*side;
		around = 4* side;
		System.out.print("Luas  = " + area + " \nKeliling = "+ around);
		System.out.println("");
}
	
	public static void triangle(){
		double base, height ;
		System.out.println("\n–>Segitiga<–");
		System.out.print("Masukkan Panjang alas : ");
		base = Input.input();
		System.out.print("Masukkan tinggi : ");
		height = Input.input();
		System.out.println("=>");
		area = 0.5 * base* height;
		around = 3 * height;
		System.out.print("Luas  = " + area + " \nKeliling = "+ around);
		System.out.println("");
}
	
	public static void rectangle(){
		double width,height;
		System.out.println("\n–>Persegi Panjang<–");
		System.out.print("Masukkan Panjang : ");
		width= Input.input();
		System.out.print("Masukkan lebar : ");
		height = Input.input();
		System.out.println("=>");
		area = width* height;
		around = 2*width+ 2*height;
		System.out.print("Luas  = " + area +" \nKeliling  = " + around);
		System.out.println("");
}
	
	public static void parallelogram(){
		double base,height;
		System.out.println("\n->jajar genjang<-");
		System.out.print("alas = ");
		base = Input.input();
		System.out.print("Tinggi = ");
		height = Input.input();
		area = base * height;
		around = base*4;
		System.out.println("Luar = "+area+"\n Keliling = "+around);
	}
	
	public static void trapezoid(){
		double baseUp,baseDown,height;
		System.out.println("\n->Trapesium<-");
		System.out.print("Alas Atas = ");
		baseUp = Input.input();
		System.out.print("Alas Bawah = ");
		baseDown = Input.input();
		System.out.print("Tinggi = ");
		height = Input.input();
		area = ((baseUp+baseDown)*height)/2;
		around = (baseUp+baseDown)+baseDown*2;
		System.out.println("Luas = "+area+"\n Keliling = "+around);
	}
	
	public static void swift(){
		double diagonal1,diagonal2;
		System.out.println("\n->Layang-Layang<-");
		System.out.print("Diagonal 1 = ");
		diagonal1 = Input.input();
		System.out.print("Diagonal 2 =");
		diagonal2 = Input.input();
		area = (diagonal1*diagonal2)/2;
		System.out.println("Luas = "+area);
		}
	}



class Input{
	private static String input;
	public static int input(){
		Scanner sc = new Scanner(System.in);
		int InputData=0;
		boolean repeat = true;
		do{
			input = sc.next();
			try{
				InputData= Integer.parseInt(input);
				repeat = false;
			}
			catch(NumberFormatException e){
				System.out.println("Inputan Salah!");
				repeat = true;
			}
		}
		while(repeat);
		return InputData;
}
}


class Menu{
	public void MenuBangunDatar(){
		int selection,menu;
		System.out.println("Menghitung Luas dan Keliling Bangun Datar");
		System.out.println("1 => lingkaran \n2.=> persegi\n3.=> segitiga\n4.=> Persegi panjang\n");
		System.out.println("5.=> Jajar Genjang \n6.=> Trapesium \n7.=>Layang-Layang\n");
		System.out.println("0.=> Keluar.");
		System.out.println("Masukan pilihan = ");
		selection = Input.input();
		int masukan=0;    
		switch(selection){
		case 1:
			BangunDatar.circle();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 2:
			BangunDatar.square();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 3:
			BangunDatar.triangle();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 4:
			BangunDatar.rectangle();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 5:
			BangunDatar.parallelogram();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 6:
			BangunDatar.trapezoid();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 7:
			BangunDatar.swift();
			System.out.println("Type 1 to return to menu top");
			System.out.println("Type 0 to finish");
			menu = Input.input();
			break;
		case 0:
			menu = 0;
		default:
			System.out.println("Salah Input!");
			menu = 1;
		}
	}
	public void MenuBangunRuang(){
		BangunRuang bangunruang = new BangunRuang();
		int menu;
		double side = 0,length=0,width=0,height=0,PHI=0,r=0;
		System.out.println("Menghitung Volume Bangun Ruang");
		System.out.println("1. Cube.");
		System.out.println("2. Beam.");
		System.out.println("3. Tube.");
		System.out.println("4. Ball.");
		System.out.println("5. Cone.");
		System.out.println("6. Pyramid.");
		System.out.println("0. Keluar.");
		int selection = Input.input();
		switch(selection){
			case 1:
				System.out.println("Enter a value : ");
				double cube = bangunruang.getCube(side);
				System.out.println(cube);
				System.out.println("Type 1 to return to menu top");
				System.out.println("Type 0 to finish");
				menu = Input.input();
				break;
			case 2:
				System.out.println("Enter a value : ");
				double beam = bangunruang.getBeam(length, width,height);
				System.out.println(beam);
				System.out.println("Type 1 to return to menu top");
				System.out.println("Type 0 to finish");
				menu = Input.input();
				break;
			case 3:
				System.out.println("Enter a value : ");
				double tube = bangunruang.getTube(PHI, r, height);
				System.out.println(tube);
				System.out.println("Type 1 to return to menu top");
				System.out.println("Type 0 to finish");
				menu = Input.input();
				break;
			case 4:
				System.out.println("Enter a value : ");
				double ball = bangunruang.getBall(PHI,r);
				System.out.println(ball);
				System.out.println("Type 1 return to menu top");
				System.out.println("Type 0 to finish");
				menu = Input.input();
				break;
			case 5:
				System.out.println("Enter a value : ");
				double cone= bangunruang.getCone(PHI,r,height);
				System.out.println(cone);
				System.out.println("Type 1 to return to menu top");
				System.out.println("Type 0 to finish");
				menu = Input.input();
				break;
			case 6:
				System.out.println("Enter a value : ");
				double pyramid = bangunruang.getPyramid(side,height);
				System.out.println(pyramid);
				System.out.println("Type 1 to return to menu top");
				System.out.println("Type 0 to finish");
				menu = Input.input();
				break;
			case 0:
				menu = 0;
			default:
				System.out.println("Salah Input!");
				menu = 1;
		}
		}
}