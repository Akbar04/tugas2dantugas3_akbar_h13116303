package TugasPP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.NumberFormatException;
public class Calculator {
	public static void main(String[] args) throws IOException{
		menu menu = new menu();
		menu.CoreMenu();
	}
}

class Symbol{
		public static float tambah(float number1, float number2){
			return(number1+number2);
		}
		public static float kurang(float number1, float number2){
			return(number1-number2);
		}
		public static double kali(float number1, float number2){
			double hasil = (double)number1*(double)number2;
			return(hasil);
		}
		public static float bagi(float number1, float number2){
			return (number1/number2);
		}
		public static float persen(float number1, float number2){
			return ((number1/number2)*100);
		}
		public static double pangkat(double number1, double number2){
			double hasil = Math.pow(number1, number2);
			return hasil;
		}
		public static float akar(float number1, float number2){
			return (float)(Math.pow(number1,number2));
		}
	}

class menu{
	private static String selection=null;
	private static int Inputan;
	
	 public static void TopMenu() throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("----- Simple Calculator -----");
			System.out.println("1. pertambahan");
			System.out.println("2. pengurangan");
			System.out.println("3. Perkalian");
			System.out.println("4. Pembagian");
			System.out.println("5. persen");
			System.out.println("6. pangkat");
			System.out.println("7. akar");
			System.out.println("0. keluar");
			System.out.print("Masukkan Inputan : ");
			selection = input.readLine();	
		}
	 static void SecondMenu() throws IOException{
		 input input = new input();
		 float x,y;
		 switch(Inputan){
		 case 1:
			 	System.out.print("nilai a = ");
				x = input.inputan();
				System.out.print("nilai b = ");
				y = input.inputan();
				System.out.println(Symbol.tambah(x, y));
				break;
		 case 2:
			 	System.out.print("nilai a = ");
				x = input.inputan();
				System.out.print("nilai b = ");
				y = input.inputan();
				System.out.println(Symbol.kurang(x, y));
				break;
		 case 3:
			 	System.out.print("nilai a = ");
				x = input.inputan();
				System.out.print("nilai b = ");
				y = input.inputan();
				System.out.println(Symbol.kali(x, y));
				break;
		 case 4:
			 	System.out.print("nilai a = ");
				x = input.inputan();
				System.out.print("nilai b = ");
				y = input.inputan();
				System.out.println(Symbol.bagi(x, y));
				break;
		 case 5:
			 	System.out.print("nilai diketahui = ");
			 	x = input.inputan();
			 	System.out.print("total nilai = ");
			 	y = input.inputan();
			 	System.out.println(Symbol.persen(x, y)+"%");
		 case 6:
			 	System.out.print("nilai a = ");
				x = input.inputan();
				System.out.print("nilai b = ");
				y = input.inputan();
				System.out.println(Symbol.pangkat(x, y));
		 case 7:
			 	System.out.print("nilai a = ");
				x = input.inputan();
				System.out.print("nilai b = ");
				y = input.inputan();
				System.out.println(Symbol.akar(x, y));
		 case 0:
			 System.out.println("Terima Kasih");
			 	break;
		default:
			System.out.println("Inputan salah!");
		 }
	 }
	
	 static void CoreMenu() throws IOException{
		 boolean TopMenu = false;
			 do{
				 do{
				 menu.TopMenu();
				 try{
					 Inputan=Integer.parseInt(selection);
					 TopMenu = false;
				 }
				 catch(NumberFormatException e){
					 System.out.println("Inputan salah");
					 TopMenu = true;
				 }
			 }while(TopMenu);
			 SecondMenu();
			 if(Inputan==0){
				 TopMenu=false;
			 }
			 else{
				 TopMenu=true
				 ;
			 }
			 }
			 while(TopMenu);
		 }
	 }
class input{
	private static float number = 0;
	public static float inputan() throws IOException{
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		boolean selama = true;
		do{
			String Mynumber = input.readLine();
			try{
				number = Float.parseFloat(Mynumber);
				selama = false;
			}
			catch(NumberFormatException e){
				System.out.println("inputan salah, Silahkan masukkan inputan lain");
				selama = true;
			}
			
		}
		while(selama);
		return number;
	}
	
	
}
