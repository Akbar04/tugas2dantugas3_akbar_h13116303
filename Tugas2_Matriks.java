import java.util.Scanner;
public class Tugas3{
	public static int Perkalian(int[][]matrixA, int[][]matrixB){
			int [][] matrixA1 = new int[3][3];
			int [][] matrixB1 = new int[3][3];
			int [][] matrixAB = new int [3][3];
			Scanner a = new Scanner (System.in);
			for(int line=0; line<3; line++){
				for(int colomn=0; colomn<3; colomn++){
					System.out.print("Value B"+(line+1)+"K"+(colomn+1)+" => ");
					matrixA1[line][colomn] = a.nextInt();
				}
				System.out.println("");
			}
			
			 
			 System.out.println("Masukkan nilai matriks B => ");
			 for(int line=0; line<3; line++){
					for(int colomn=0; colomn<3; colomn++){
						System.out.print("Nilai B"+(line+1)+"K"+(colomn+1)+" => ");
						matrixB1[line][colomn] = a.nextInt();
					}
					System.out.println("");
				}
			 
			 for (int line = 0; line<3; line++){
				 if (line == 0){
					 System.out.println("Nilai matrix A      X    matrix B");
				 }
				 System.out.print("|");
				 for (int colomn = 0; colomn<3; colomn++){
					 
					 System.out.print(matrixB1[line][colomn]+"  ");
				 }
				 System.out.print("|");
				 System.out.print("     ");
				 System.out.print("|");
				 for (int colomn = 0; colomn<3; colomn++){
					 System.out.print(matrixA1[line][colomn]+"  ");
				 }
				 System.out.print("|");
				 System.out.println("");
			 }
			 
			 matrixAB[0][1] = matrixA1[0][0]*matrixB1[0][1]+matrixA1[0][1]*matrixB1[1][1]+matrixA1[0][2]*matrixB1[2][1]; 
			 matrixAB[0][0] = matrixA1[0][0]*matrixB1[0][0]+matrixA1[0][1]*matrixB1[1][0]+matrixA1[0][2]*matrixB1[2][0];
			 matrixAB[0][2] = matrixA1[0][0]*matrixB1[0][2]+matrixA1[0][1]*matrixB1[1][2]+matrixA1[0][2]*matrixB1[2][2];
			 
			 matrixAB[1][0]= matrixA1[1][0]*matrixB1[0][0]+matrixA1[1][1]*matrixB1[1][0]+matrixA1[1][2]*matrixB1[2][0];
			 matrixAB[1][1]= matrixA1[1][0]*matrixB1[0][1]+matrixA1[1][1]*matrixB1[1][1]+matrixA1[1][2]*matrixB1[2][1];
			 matrixAB[1][2]= matrixA1[1][0]*matrixB1[0][2]+matrixA1[1][1]*matrixB1[1][2]+matrixA1[1][2]*matrixB1[2][2];
			 
			 matrixAB[2][0]= matrixA1[2][0]*matrixB1[0][0]+matrixA1[2][1]*matrixB1[1][0]+matrixA1[2][2]*matrixB1[2][0];
			 matrixAB[2][1]= matrixA1[2][0]*matrixB1[0][1]+matrixA1[2][1]*matrixB1[1][1]+matrixA1[2][2]*matrixB1[2][1];
			 matrixAB[2][2]= matrixA1[2][0]*matrixB1[0][2]+matrixA1[2][1]*matrixB1[1][2]+matrixA1[2][2]*matrixB1[2][2];
			 
			 System.out.println("Hasil perkalian dari nilai M X N ==>");
			 
			 for (int line = 0; line<3; line++){
				 System.out.print("|");
				 for (int colomn = 0;colomn<3;colomn++){
					 System.out.print(matrixAB[line][colomn]+"  ");
				 }
				 System.out.print("|");
				 System.out.println("");
			 }
			int line = 0;
			int colomn = 0;
			return matrixAB[line][colomn];
	}
	public static int penjumlahanPengurangan(int [][] matrixA,int[][] matrixB){
        Scanner input = new Scanner(System.in);

        int line;
        int colomn = 0;
        int[][]matrixA1 = new int[3][3];
        int[][]matrixB1 = new int[3][3];
        int[][]jumlahMatrix = new int[3][3];
        int[][]kurangMatrix= new int[3][3];


        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                System.out.print("Matrix A  " + line+ "." + colomn+ "= ");
                matrixA1[line][colomn] =input.nextInt();
            }
        }

        System.out.println();


        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                System.out.print("Matrix B  " + line+ "." + colomn + "= ");
                matrixB1[line][colomn] =input.nextInt();
            }
        }


        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                jumlahMatrix[line][colomn] = matrixA1[line][colomn] + matrixB1[line][colomn];
                kurangMatrix[line][colomn] = matrixA1[line][colomn] - matrixB1[line][colomn];
            }
        }

        System.out.println("\nMatrix A\n");
        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                System.out.print(" " + matrixA1[line][colomn]);
            }
            System.out.println();
        }

        System.out.println("\nMatrix B\n");
        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                System.out.print("   " + matrixB1[line][colomn]);
            }
            System.out.println();
        }

        System.out.println("\nMatrixA + Matrix B\n");
        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                System.out.print(" " + jumlahMatrix[line][colomn]);
            }
            System.out.println();
        }

        System.out.println("\nMatrix A - Matrix B\n");
        for(line=0;line<3;line++)
        {
            for(colomn=0;colomn<3;colomn++)
            {
                System.out.print(" " + kurangMatrix[line][colomn]);
            }
            System.out.println();
        }
		return jumlahMatrix[line][colomn];
	}
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int menu;
		int[][]matrixA=new int [3][3];
        int[][]matrixB=new int [3][3];
		
		do{
			System.out.println("Perkalian matriks ordo 3 X 3");
			System.out.println("1. Penjumlahan dan pengurangan");
			System.out.println("2. Perkalian");
			System.out.println("3. determinant");
			System.out.print("input = ");
			menu = sc.nextInt();
			
			switch(menu){
				case 1:
					System.out.println(Tugas3.penjumlahanPengurangan(matrixA,matrixB));
					System.out.println("1 untuk ke menu awal, 0 untuk keluar");
					System.out.print("input = ");
					menu = sc.nextInt();
				case 2:
					System.out.println(Tugas3.Perkalian(matrixA,matrixB));
					System.out.println("1 untuk ke menu awal, 0 untuk keluar");
					System.out.print("input = ");
					menu = sc.nextInt();
				case 3:
					int a,b;
					int [][] matrix = new int[3][3];
					int [][] Matrix = new int [3][3];
					System.out.println("==>Mancari Determinan suatu Matriks<==");
					System.out.println("Masukkan nilai M ==>");
					System.out.println("Nilai B1K1  : ");
					 matrix[0][0] = sc.nextInt();
					 System.out.println("Nilai B1K2  : ");
					 matrix[0][1] = sc.nextInt();
					 System.out.println("Nilai B1K3  : ");
					 matrix[0][2] = sc.nextInt();
					 System.out.println("Nilai B2K1  : ");
					 matrix[1][0] = sc.nextInt();
					 System.out.println("Nilai B2K2  : ");
					 matrix[1][1] = sc.nextInt();
					 System.out.println("Nilai B2K3  : ");
					 matrix[1][2] = sc.nextInt();
					 System.out.println("Nilai B3K1  : ");
					 matrix[2][0] = sc.nextInt();
					 System.out.println("Nilai B3K2  : ");
					 matrix[2][1] = sc.nextInt();
					 System.out.println("Nilai B3K3  : ");
					 matrix[2][2] = sc.nextInt();
					 
					 System.out.println("Nilai data M");
					 for (int x = 0; x <3; x++){
						 for(int y = 0; y<3; y++){
							 System.out.print(matrix[x][y]+" ");
						 }
						 System.out.println(" ");
					 }
					 a = matrix[0][0]*matrix[1][1]*matrix[2][2] + matrix[0][1]*matrix[1][2]*matrix[2][0] + matrix[0][2]*matrix[1][0]*matrix[2][1];
					 b = matrix[0][2]*matrix[1][1]*matrix[2][0] + matrix[1][2]*matrix[2][1]*matrix[0][0] + matrix[2][2]*matrix[1][0]*matrix[0][1];
					 double c = a - b;
					 System.out.print("determinan nya ==> "+ c);
					 System.out.println("");
					 
					 System.out.println("1 untuk ke menu awal, 0 untuk keluar");
						System.out.print("input = ");
						menu = sc.nextInt();
					
			}
		
	}
		while(menu>=1);
			System.out.println("Makasih");
	}
	}
