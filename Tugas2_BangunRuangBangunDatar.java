package PP5;
import java.util.Scanner;
public class Tugas1{
		public static int Cube(int side){
			Scanner sc = new Scanner(System.in);
			System.out.print("Side = ");
			side = sc.nextInt();
			int Volume = side * side * side;
			System.out.println("Result = ");
			return Volume;
		}
		public static int Beam(float length, float width,float height){
			Scanner sc = new Scanner(System.in);
			System.out.print("Length = ");
			length = sc.nextFloat();
			System.out.print("Width = ");
			width = sc.nextFloat();
			System.out.print("Height = ");
			height = sc.nextFloat();
			int Volume = (int)length * (int)width * (int)height;
			System.out.print("Result = ");
			return Volume;
		}
		public static float Tube(float PHI, float r, float height){
			Scanner sc = new Scanner(System.in);
			PHI = (float)3.14;
			System.out.print("Fingers = ");
			r = sc.nextFloat();
			System.out.print("Height = ");
			height = sc.nextFloat();
			float Volume = PHI * r * r * height;
			System.out.print("Result = ");
			return Volume;
		}
		public static float Ball(float PHI, float r){
			Scanner sc = new Scanner(System.in);
			PHI = (float)3.14;
			System.out.print("Fingers = ");
			r = sc.nextFloat();
			float Volume = 4/3 * PHI * r * r;
			System.out.print("Result = ");
			return Volume;
		}
		public static float Cone(float PHI, float r, float t){
			Scanner sc = new Scanner(System.in);
			PHI = (float) 3.14;
			System.out.print("Fingers = ");
			r = sc.nextFloat();
			System.out.print("Height = ");
			t = sc.nextFloat();
			float Volume= 1/3 * PHI * r * r * t;
			System.out.println("Result = ");
			return Volume;
		}
		public static float Pyramid(float PHI, int side, float	 height){
			Scanner sc = new Scanner(System.in);
			PHI = (float) 3.14;
			System.out.print("Side = ");
			side = sc.nextInt();
			System.out.print("Height = ");
			height = sc.nextInt();
			float Volume = 1/3 * (float)(side*side)*(float)height;
			System.out.println("Result = ");
			return Volume;
		}
		public void lingkaran()   
{
        Scanner input = new Scanner(System.in);
        double phi = 3.14;
        double r, luas,keliling;
        System.out.println("\n–> lingkaran <–");
        System.out.print("Masukkan Panjang Jari-jari : ");
        r = input.nextDouble();
        System.out.println("=>");
        luas = phi * r * r;
        keliling = 2 * phi * r;
        System.out.print("Luas  = " + (int)luas +  "\nKeliling  = "+ (int)keliling);
        System.out.println("");
        }
public void persegi()
 {
        Scanner input = new Scanner(System.in);
        double s, luas,keliling;
        System.out.println("\n–>Persegi<–");
        System.out.print("Masukkan Panjang sisi : ");
        s = input.nextDouble();
        System.out.println("=>");
        luas = s * s;
        keliling = 4* s;
        System.out.print("Luas  = " + (int)luas + " \nKeliling = "+ (int)keliling);
        System.out.println("");
        }
    public void segitiga()
    {
        Scanner input = new Scanner(System.in);
        double a , t, luas,keliling;
        System.out.println("\n–>Segitiga<–");
        System.out.print("Masukkan Panjang alas : ");
        a = input.nextDouble();
        System.out.print("Masukkan tinggi : ");
        t = input.nextDouble();
        System.out.println("=>");
        luas = 0.5 * a * t;
        keliling = 3 * t;
        System.out.print("Luas  = " + (int)luas + " \nKeliling = "+ (int)keliling);
        System.out.println("");
        }
public void ppanjang()
    {
        Scanner input = new Scanner(System.in);
        double p , l, luas,keliling;
        System.out.println("\n–>Persegi Panjang<–");
        System.out.print("Masukkan Panjang : ");
        p = input.nextDouble();
        System.out.print("Masukkan lebar : ");
        l = input.nextDouble();
        System.out.println("=>");
        luas = p * l;
        keliling = 2*p + 2*l;
        System.out.print("Luas  = " + (int)luas + " \nKeliling  = " + (int)keliling);
        System.out.println("");
        }

		public static void main(String[] args){
			Scanner sc = new Scanner(System.in);
			float lenght=0,width = 0,height=0,PHI=0,r=0;
			int side = 0, menu=1;
			do{
				System.out.println("");
				System.out.println("menu");
				System.out.println("1. Bangun datar");
				System.out.println("2. Bangun ruang");
				int selection = sc.nextInt();
				switch(selection){
				case 1:
					Tugas1 yudana = new Tugas1 ();
						double a;
						System.out.println("Menghitung Luas dan Keliling Bangun Datar");
						System.out.println("1 => lingkaran \n2.=> persegi\n3.=> segitiga\n4.=> Persegi panjang\n");
						System.out.println("Masukan pilihan = ");
						a = sc.nextDouble();
						int masukan=0;    
						if (a==1){
							yudana.lingkaran();
							System.out.println("Type 1 to return to menu top");
							System.out.println("Type 0 to finish");
							menu = sc.nextInt();}
						else if (a==2){
							yudana.persegi();
							System.out.println("Type 1 to return to menu top");
							System.out.println("Type 0 to finish");
							menu = sc.nextInt();}
						else if (a==3){
							yudana.segitiga();
							System.out.println("Type 1 to return to menu top");
							System.out.println("Type 0 to finish");
							menu = sc.nextInt();}
						else if (a==4){
							yudana.ppanjang();
							System.out.println("Type 1 to return to menu top");
							System.out.println("Type 0 to finish");
							menu = sc.nextInt();}
						else {
						System.out.println("pilihan salah");
						System.out.println("Type 1 to return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();}
				case 2:
				System.out.println("menu");
				System.out.println("1. Cube.");
				System.out.println("2. Beam.");
				System.out.println("3. Tube.");
				System.out.println("4. Ball.");
				System.out.println("5. Cone.");
				System.out.println("6. Pyramid.");
				int selection = sc.nextInt();
				switch(selection){
					case 1:
						System.out.println("Enter a value : ");
						int x = Tugas1.Cube(side);
						System.out.println(x);
						System.out.println("Type 1 to return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();
						break;
					case 2:
						System.out.println("Enter a value : ");
						int y = Tugas1.Beam(lenght, width, height);
						System.out.println(y);
						System.out.println("Type 1 to return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();
						break;
					case 3:
						System.out.println("Enter a value : ");
						float z= Tugas1.Tube(PHI,r,height);
						System.out.println(z);
						System.out.println("Type 1 to return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();
						break;
					case 4:
						System.out.println("Enter a value : ");
						float a= Tugas1.Ball(PHI,r);
						System.out.println(a);
						System.out.println("Type 1 return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();
						break;
					case 5:
						System.out.println("Enter a value : ");
						float b= Tugas1.Cone(PHI,r,height);
						System.out.println(b);
						System.out.println("Type 1 to return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();
						break;
					case 6:
						System.out.println("Enter a value : ");
						float c= Tugas1.Pyramid(PHI,side,height);
						System.out.println(c);
						System.out.println("Type 1 to return to menu top");
						System.out.println("Type 0 to finish");
						menu = sc.nextInt();
						break;
				}
				}
			}
			while(menu >= 1);
				System.out.println("Terima kasih atas kerja samanya :)");
		}
}
